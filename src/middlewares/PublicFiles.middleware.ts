import { NextFunction, Request, Response } from 'express';
import { HttpBadRequest, HttpForbidden } from '../httpErrors';
import User from '../models/User';
import FolderService from '../services/Folder.service';
import UserService from '../services/User.service';

const folderService = new FolderService();
const userService = new UserService();
export default async function PublicFilesMiddleware(
  req: Request & {user?: User},
  res: Response,
  next: NextFunction,
) {
  const path = req.query.path || req.query.fileKey;
  if (!path) {
    return next(new HttpBadRequest('Unrecognized path parameter'));
  }
  const isAuthExists = await userService.getByToken(req.headers.authorization || req.cookies?.['X-AUTH']);

  if (isAuthExists) {
    return next();
  }
  const isPublicPath = await folderService.isPublicPath(path.toString());
  if (isPublicPath) {
    return next();
  }
  return next(new HttpForbidden());
}

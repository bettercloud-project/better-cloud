import { Request, Response, NextFunction } from 'express';
import { HttpUnauthorized } from '../httpErrors';
import User from '../models/User';
import UserService from '../services/User.service';

const userService = new UserService();

export default async function AuthMiddleware(
  req: Request & {user?: User},
  res: Response, next: NextFunction,
) {
  try {
    const user = await userService.getByToken(req.headers.authorization || req.cookies['X-AUTH']);
    if (!user) {
      return next(new HttpUnauthorized());
    }
    req.user = user;
    res.cookie('X-AUTH', req.headers.authorization, {
      secure: true,
      expires: new Date('2090.01.10'),
      sameSite: 'none',
      signed: false,
    });
    return next();
  } catch (err) {
    return next(new HttpUnauthorized());
  }
}

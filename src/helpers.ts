import { Application } from 'express';
import { ManagedUpload } from 'aws-sdk/clients/s3';
import bytes from 'bytes';

function split(thing: any): string {
  if (typeof thing === 'string') {
    return thing;
  } if (thing.fast_slash) {
    return '';
  }
  const match = thing.toString()
    .replace('\\/?', '')
    .replace('(?=\\/|$)', '$')
    .match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//);
  return match
    ? match[1].replace(/\\(.)/g, '$1')
    : `<complex:${thing.toString()}>`;
}

function getRoutesOfLayer(path: string, layer: any): string[] {
  if (layer.method) {
    return [`${layer.method.toUpperCase()} ${path}`];
  }
  if (layer.route) {
    return getRoutesOfLayer(path + split(layer.route.path), layer.route.stack[0]);
  }
  if (layer.name === 'router' && layer.handle.stack) {
    let routes: string[] = [];

    layer.handle.stack.forEach((stackItem: any) => {
      routes = routes.concat(getRoutesOfLayer(path + split(layer.regexp), stackItem));
    });

    return routes;
  }

  return [];
}


// eslint-disable-next-line import/prefer-default-export
export function getRoutes(app: Application): string[] {
  let routes: string[] = [];

  // eslint-disable-next-line no-underscore-dangle
  app._router.stack.forEach((layer: any) => {
    routes = routes.concat(getRoutesOfLayer('', layer));
  });

  return routes;
}

export function calculateProgress(progress: ManagedUpload.Progress): string {
  if (!progress.total) {
    return bytes(progress.loaded);
  }

  return `${Math.round((progress.loaded / progress.total) * 100)}%`;
}

export function prepareS3Path(path: string, fileName: string): string {
  if (path === '/') {
    return fileName;
  }
  if (path.endsWith('/')) {
    return `${path}${fileName}`;
  }

  return `${path}/${fileName}`;
}

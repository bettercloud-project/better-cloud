import { NextFunction, Request, Response } from 'express';
import Folder from '../models/Folder';
import FolderService from '../services/Folder.service';
import BaseController from './Base.controller';
import User from '../models/User';
import { HttpBadRequest } from '../httpErrors';

export default class FolderController extends BaseController<Folder> {
  constructor() {
    const folderService = new FolderService();
    super(folderService);
  }

  findMy = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { user } = (req as unknown as {user: User});
      const folders = await this.service.findMany({ userId: user!.id });
      res.send(folders);
    } catch (error) {
      next(error);
    }
  };

  getOneMy = async (req: Request, res: Response, next: NextFunction) => {
    const id = Number(req.params.id);

    if (Number.isNaN(id)) {
      return next(new HttpBadRequest('Wrong id'));
    }
    try {
      const { user } = (req as unknown as {user: User});
      const folder = await this.service.findOne({ userId: user!.id, id });
      res.send(folder);
    } catch (error) {
      next(error);
    }
    return null;
  };

  createMy = async (req: Request, res: Response, next: NextFunction) => {
    const entity = req.body;
    if (!entity) {
      return next(new HttpBadRequest());
    }
    try {
      const { user } = (req as unknown as {user: User});
      const folders = await this.service.create({ ...entity, userId: user!.id });
      res.send(folders);
    } catch (error) {
      next(error);
    }
    return null;
  };

  updateOneMy = async (req: Request, res: Response, next: NextFunction) => {
    const entity = req.body;
    if (!entity) {
      return next(new HttpBadRequest());
    }
    try {
      const { user } = (req as unknown as {user: User});
      const { id, ...folder } = entity;
      const folders = await this.service.update(id, { ...folder, userId: user!.id });
      res.send(folders);
    } catch (error) {
      next(error);
    }
    return null;
  };

  deleteOneMy = async (req: Request, res: Response, next: NextFunction) => {
    const id = Number(req.params.id);
    const { user } = (req as unknown as {user: User});

    if (Number.isNaN(id)) {
      return next(new HttpBadRequest('Wrong id'));
    }
    const myFolder = await this.service.findOne({ userId: user!.id, id });
    if (!myFolder) {
      return next(new HttpBadRequest('not your entity'));
    }

    try {
      const result = await this.service.delete(id);
      res.send(result.toString());
    } catch (err: any) {
      return next(new HttpBadRequest(err.message));
    }
    return null;
  };
}

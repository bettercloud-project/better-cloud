import UserGroupService from '../services/UserGroup.service';
import BaseController from './Base.controller';
import UserGroup from '../models/UserGroup';

export default class UserGroupController extends BaseController<UserGroup> {
  constructor() {
    const userGroupService = new UserGroupService();
    super(userGroupService);
  }
}

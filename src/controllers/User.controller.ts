import BaseController from './Base.controller';
import User from '../models/User';
import UserService from '../services/User.service';

export default class UserController extends BaseController<User> {
  constructor() {
    super(new UserService());
  }
}

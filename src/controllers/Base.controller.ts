import { Request, Response, NextFunction } from 'express';
import BaseService from '../services/BaseObjection.service';
import { HttpBadRequest } from '../httpErrors';

export default class BaseController<ModelType extends Record<string, any>> {
  constructor(protected readonly service: BaseService<ModelType>) { }

  getAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await this.service.getAll();
      res.send(data);
    } catch (error) {
      next(error);
    }
  };

  getOne = async (req: Request, res: Response, next: NextFunction) => {
    const id = Number(req.params.id);

    if (Number.isNaN(id)) {
      return next(new HttpBadRequest('Wrong id'));
    }

    try {
      const entity = await this.service.getById(id);
      res.send(entity);
    } catch (error) {
      next(error);
    }
    return null;
  };

  updateOne = async (req: Request, res: Response, next: NextFunction) => {
    const entity = req.body;
    if (!entity || !req.params.id) {
      return next(new HttpBadRequest());
    }
    const id = Number(req.params.id);
    const oldGroup = await this.service.getById(id);

    if (!oldGroup) {
      return next(new HttpBadRequest('Wrong id'));
    }

    try {
      const userGroups = await this.service.update(id, entity);
      res.send(userGroups);
    } catch (err: any) {
      next(new HttpBadRequest(err.message));
    }
    return null;
  };

  createOne = async (req: Request, res: Response, next: NextFunction) => {
    const entity = req.body;
    if (!entity) {
      return next(new HttpBadRequest());
    }

    try {
      const newEntity = await this.service.create(entity);
      res.send(newEntity);
    } catch (err: any) {
      return next(new HttpBadRequest(err.message));
    }
    return null;
  };

  deleteOne = async (req: Request, res: Response, next: NextFunction) => {
    const id = Number(req.params.id);

    if (Number.isNaN(id)) {
      return next(new HttpBadRequest('Wrong id'));
    }

    try {
      const result = await this.service.delete(id);
      res.send(result);
    } catch (err: any) {
      return next(new HttpBadRequest(err.message));
    }
    return null;
  };
}

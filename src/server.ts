import 'dotenv/config';
import express, { Request, Response } from 'express';
import cors from 'cors';
import morgan from 'morgan';
import Knex from 'knex';
import { Model } from 'objection';
import cookieParser from 'cookie-parser';

import routes from './routes';
import { getRoutes } from './helpers';
import { ProcessEnv } from './typeHelpers';

const knexConfig = require('../knexfile');

const knex = Knex(knexConfig.development);

const config = process.env as ProcessEnv;

// Bind all Models to a knex instance. If you only have one database in
// your server this is all you have to do. For multi database systems, see
// the Model.bindKnex() method.
Model.knex(knex);

const app = express();
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); //
app.use(cookieParser());
app.use(cors({ origin: new RegExp(config.ALLOWED_HOSTS), credentials: true }));
app.use(morgan('combined'));

app.use('/api/v1', routes);

app.use((err: Error, req: Request, res: Response, _next: any) => {
  const { message } = err;
  if (message && message.length) {
    const [status, ...text] = message.split(' ');
    if (!Number.isNaN(Number(status))) {
      return res.status(Number(status)).send(text.join(' '));
    }
  }
  // eslint-disable-next-line no-console
  console.error(err.stack);
  res.status(500).send(message);
});

// eslint-disable-next-line no-console
console.log(getRoutes(app));

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is running in http://localhost:${PORT}`);
});

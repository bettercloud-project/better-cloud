import { Model } from 'objection';
import bcrypt from 'bcrypt';
import UserGroup from './UserGroup';

export default class User extends Model {
  static tableName = 'users';

  id!: number;
  email!: string;
  firstName?: string;
  lastName?: string;
  groups?: UserGroup[];
  password?: string;

  created_at!: string;
  updated_at!: string;

  $beforeInsert() {
    this.created_at = new Date().toISOString();
    this.updated_at = new Date().toISOString();
    this.password = bcrypt.hashSync(this.password, 12);
  }

  async $beforeUpdate() {
    this.updated_at = new Date().toISOString();
    if (this.password) {
      this.password = await bcrypt.hash(this.password, 12);
    }
  }

  $formatJson(json: User) {
    const PROTECTED_FIELDS = ['password'];

    const formattedJson = Object.fromEntries(
      Object.entries(json).filter(([key]) => !PROTECTED_FIELDS.includes(key)),
    ) as User;

    return super.$formatJson(formattedJson);
  }


  static relationMappings = () => ({
    groups: {
      relation: Model.ManyToManyRelation,
      modelClass: UserGroup,
      join: {
        from: 'users.id',
        through: {
          from: 'users_userGroups.userId',
          to: 'users_userGroups.groupId',
        },
        to: 'userGroups.id',
      },
    },
  });
}

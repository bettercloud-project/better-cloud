import { Model } from 'objection';

export default class UserGroup extends Model {
  static tableName = 'userGroups';

  id!: number;
  name!: 'user' | 'admin' | 'guest';
  created_at!: string;
  updated_at!: string;

  $beforeInsert() {
    this.created_at = new Date().toISOString();
    this.updated_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }
}

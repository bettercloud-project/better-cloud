import { Model } from 'objection';
// import User from './User';

export default class Folder extends Model {
  static tableName: string = 'folder';

  id!: number;

  path!: string;

  userId!: number;

  type!: 'public' | 'private';

  created_at!: string;

  updated_at!: string;

  $beforeInsert() {
    this.created_at = new Date().toISOString();
    this.updated_at = new Date().toISOString();
  }

  $beforeUpdate() {
    this.updated_at = new Date().toISOString();
  }

  // static relationMappings = {
  //   user: {
  //     relation: Model.BelongsToOneRelation,
  //     modelClass: User,
  //     join: {
  //       from: 'folder.userId',
  //       to: 'users.id',
  //     },
  //   },
  // };
}

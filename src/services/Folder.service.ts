import Folder from '../models/Folder';
import BaseService from './BaseObjection.service';

export default class FolderService extends BaseService<Folder> {
  constructor() {
    super(Folder);
  }

  async isPublicPath(path: string) {
    const folders = await this.findMany({ type: 'public' });
    const publicFolders = folders.map((f) => f.path);
    const isPublic = publicFolders.some((p) => path.startsWith(p));
    return isPublic;
  }
}

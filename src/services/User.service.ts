import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import BaseService from './BaseObjection.service';
import User from '../models/User';
import { ProcessEnv } from '../typeHelpers';

export type LoginRequestBody = Record<'email' | 'password', string>;

const { JWT_SECRET } = process.env as ProcessEnv;

export default class UserService extends BaseService<User> {
  // relations: string[];

  constructor() {
    // this.relations = Object.keys(User.getRelations());
    super(User);
    // console.log(this.relations));
  }

  getByLogin = async ({ email, password } : LoginRequestBody) => {
    const user = await this.findOne({ email });
    if (!user) return null;
    const match = await bcrypt.compare(password, user.password || '');
    if (!match) return null;

    return user;
  };

  async getByToken(token?: string) {
    if (!token) {
      return null;
    }
    const userFromToken = jwt.verify(token.split(' ')[1] || token, JWT_SECRET) as (User | undefined);
    if (!userFromToken) {
      return null;
    }
    if (!userFromToken.id) {
      return null;
    }
    return this.getById(userFromToken.id);
  }
}

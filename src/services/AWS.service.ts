/* eslint-disable class-methods-use-this */
import { S3 } from 'aws-sdk';
import { Readable } from 'stream';
import { HttpHeaders } from 'aws-sdk/clients/iot';
import { Body } from 'aws-sdk/clients/s3';
import RedisService from './Redis.service';
import { calculateProgress } from '../helpers';

type FileResponse = Partial<S3.Object> & {
  name: string,
  type: 'file' | 'directory',
};
export default class AWSService {
  private readonly self = AWSService;
  private readonly s3: S3;
  // private readonly s4: S3;

  private readonly bucketName: string;

  constructor() {
    this.s3 = new S3({
      accessKeyId: process.env.S3_ACCESS_KEY,
      secretAccessKey: process.env.S3_SECRET_KEY,
    });
    this.bucketName = process.env.S3_BUCKET || '';
  }

  async listFiles(path: string, params?: Omit<S3.ListObjectsV2Request, 'Bucket' | 'Prefix'>) {
    const key = `${path}_${Object.entries(params || {}).flat().join('_')}`;
    console.log('REQ KEY = ', key);

    const cache = await RedisService.get(key);
    if (cache) {
      console.log('From Redis');

      return Promise.resolve(JSON.parse(cache) as S3.ListObjectsV2Output);
    }
    console.log('NOT FROM REDIS');

    const Prefix = path.charAt(0) === '/' ? path.slice(1) : path;
    const result = await this.s3
      .listObjectsV2({
        Bucket: this.bucketName, Prefix, Delimiter: params?.Delimiter || '/', ...params,
      })
      .promise();

    RedisService.set(key, JSON.stringify(result), 'EX', 3600);

    return result;
  }

  private s3ResponseToFilesAndDirs({
    CommonPrefixes = [],
    Contents = [],
    ...info
  } : S3.ListObjectsV2Output): FileResponse[] {
    const directories: FileResponse[] = CommonPrefixes.map(({ Prefix = '' }) => ({
      name: Prefix.replace(info.Prefix!, '').slice(0, -1),
      Key: Prefix,
      type: 'directory',
    }));
    const files: FileResponse[] = Contents.map((file) => ({
      ...file,
      name: file.Key?.replace(info.Prefix || '', '') || '',
      type: 'file',
    }));
    return directories.concat(files);
  }

  async listFilesAndDirs(path: string) {
    const data = await this.listFiles(path);

    return {
      ...data,
      data: this.s3ResponseToFilesAndDirs(data),
    };
  }

  async listObjectsDeep(path: string): Promise<S3.ListObjectsV2Output & {data: FileResponse[]}> {
    const objects: FileResponse[] = [];
    let info: S3.ListObjectsV2Output = {
      KeyCount: 0,
    };

    return new Promise((resolve) => {
      const list = async (token?: string) => {
        const resp = await this.listFiles(path, { ContinuationToken: token, MaxKeys: 1000 });
        info = {
          ...resp,
          KeyCount: (info?.KeyCount || 0) + (resp?.KeyCount || 0),
        };
        delete info.CommonPrefixes;
        delete info.Contents;
        objects.push(...this.s3ResponseToFilesAndDirs(resp));
        if (resp.IsTruncated) {
          await list(resp.NextContinuationToken);
          // await list([...resp.Contents!].pop()?.Key);
        } else {
          resolve({ ...info, data: this.sortObjects(objects) });
        }
      };

      list();
    });
  }

  sortObjects(objects: FileResponse[]): FileResponse[] {
    return objects.sort((a, b) => a.type.localeCompare(b.type));
  }

  async getFile(key: string) {
    const cache = await RedisService.get(key);
    if (cache) {
      console.log('FILE From Redis');
      const json = JSON.parse(cache) as (Omit<S3.GetObjectOutput, 'Body'> & { Body: string});
      return Promise.resolve({ ...json, Body: Buffer.from(json.Body) });
    }
    console.log('FILE NOT FROM REDIS');

    const file = await this.s3.getObject({ Bucket: this.bucketName, Key: key }).promise();
    RedisService.set(key, JSON.stringify({ ...file, Body: file.Body?.toString() }), 'EX', 36000);
    // console.log(file);
    return file;
  }

  async uploadFile(filePath: string, file: Body): Promise<S3.ManagedUpload.SendData> {
    // this.s3.getSignedUrl
    // return Promise.resolve();
    const path = this.self.getPathFromKey(filePath);
    const result = await this.s3.upload({
      Bucket: this.bucketName,
      Key: filePath,
      Body: file,
    }).on('httpUploadProgress', (progress) => {
      console.log('loaded - ', calculateProgress(progress));
    }).promise();
    await RedisService.removeKeysLike(path);

    return result;
  }

  async removeFile(fileKey: string) {
    const path = this.self.getPathFromKey(fileKey);
    await this.s3.deleteObject({
      Bucket: this.bucketName,
      Key: fileKey,
    }).promise();

    await RedisService.removeKeysLike(path);
    return { Key: fileKey };
  }

  async downloadStream(
    key: string,
    range?: string,
  ): Promise<{ stream: Readable, headers: HttpHeaders}> {
    const request = this.s3.getObject({ Bucket: this.bucketName, Key: key, Range: range });
    return new Promise((resolve) => {
      request.on('httpHeaders', (statusCode, headers, response) => {
        resolve({ stream: response.httpResponse.createUnbufferedStream() as Readable, headers });
      }).send();
    });
  }

  static getPathFromKey(awsKey: string) {
    return awsKey.split('/').slice(0, -1).join('/').concat('/');
  }
}

import BaseService from './BaseObjection.service';
import UserGroup from '../models/UserGroup';

export default class UserGroupService extends BaseService<UserGroup> {
  constructor() {
    super(UserGroup);
  }
}

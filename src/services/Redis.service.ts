import * as redis from 'redis';
import { promisify } from 'util';
import * as redisMock from 'redis-mock';

class RedisService {
  private readonly redisClient: redis.RedisClient;

  constructor() {
    const host = process.env.REDIS_HOST;
    if (host) {
      this.redisClient = redis.createClient({ host });
    } else {
      this.redisClient = redisMock.createClient();
    }
    this.redisClient.on('error', (err) => {
      console.error(err);
    });
    // this.redisClient.
  }

  /**
   *
   * @description invalidate redis cache for recently modified entries
   */
  async removeKeysLike(key: string): Promise<number> {
    console.log('Path to cleanup - ', key);

    const affectedKeys = await this.keys(`${key}_*`);
    console.log('INVALIDATE KEYS - ', affectedKeys);

    if (affectedKeys.length === 0) {
      return 0;
    }

    return new Promise((resolve, reject) => {
      this.redisClient.del(affectedKeys, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  private get keys() {
    return promisify(this.redisClient.keys).bind(this.redisClient);
  }

  private get del() {
    return promisify(this.redisClient.del).bind(this.redisClient);
  }

  get get() {
    return promisify(this.redisClient.get).bind(this.redisClient);
  }

  // set = promisify(this.redisClient.set).bind(this.redisClient);
  get set() {
    return promisify(
      (key: string,
        value: string,
        mode?: string,
        duration?: number,
        cb?: redis.Callback<'OK' | undefined>) => this.redisClient.set(key, value, mode || 'EX', duration || 3600, cb),
    ).bind(this.redisClient);
  }
}

export default new RedisService();

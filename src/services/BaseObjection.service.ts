import { RelationExpression, Model } from 'objection';

export default class BaseService<T extends Record<string, any>> {
  private readonly relations: string[];
  // private readonly relationExpression: string;

  constructor(protected readonly model: typeof Model) {
    this.relations = Object.keys(this.model.getRelations());
    // this.relationExpression = `[${this.relations.toString()}]`;
  }

  getAll = () => this.model.query().withGraphFetched('*') as unknown as Promise<T[]>;

  getById = (id: number) => this.model.query().findById(id).withGraphFetched('*').execute() as unknown as Promise<T & Model>;

  create(model: Omit<T, 'id'>) {
    return this.model.query().insertWithRelated(model).execute() as unknown as Promise<T>;
  }

  delete = async (id: number) => {
    await this.model.query().deleteById(id).execute();
    return id;
  };

  update = async (id:number, entity: T) => {
    const relationKeysInEntity = Object.keys(entity).filter((key) => this.relations.includes(key));
    if (relationKeysInEntity.length) {
      // console.log('We have to update relations');
      // TODO: should we update the dependencies?
      // const oldEntity = await this.getById(id);

      // const getIdsByKey = (key: string): number[] => (entity[key] && Array.isArray(entity[key])
      //   ? entity[key].map((rel: any) => rel.id)
      //   : [entity[key].id]);

      // await Promise.all(
      //   relationKeysInEntity.map((key) => this.model.relatedQuery(key).for(id).relate(getIdsByKey(key))),
      // );
      throw new Error('Entities with relations not supported for now');
    } else {
      await this.model.query().update(entity).execute();
    }
    return this.getById(id);
  };

  findOne = (entity: Partial<T>, relationsGraph?: RelationExpression<Model>) => this.model.query()
    .findOne(entity)
    .withGraphFetched(relationsGraph || '*')
    .execute() as unknown as Promise<T & Model>;

  findMany = (entity: Partial<T>) => this.model
    .query().where(entity).execute() as unknown as Promise<T[]>;
}

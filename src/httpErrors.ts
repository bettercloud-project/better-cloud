/* eslint-disable max-classes-per-file */
export class HttpBadRequest extends Error {
  constructor(message: string = 'Bad Request') {
    super(`400 ${message}`);
  }
}

export class HttpUnauthorized extends Error {
  constructor(message: string = 'Unauthorized') {
    super(`401 ${message}`);
  }
}
export class HttpForbidden extends Error {
  constructor(message: string = 'Forbidden') {
    super(`403 ${message}`);
  }
}

export class HttpServerError extends Error {
  constructor(message: string = 'Internal Server Error') {
    super(`500 ${message}`);
  }
}
export class HttpNotFoundError extends Error {
  constructor(message: string = 'Not found') {
    super(`404 ${message}`);
  }
}

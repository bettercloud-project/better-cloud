import { Router } from 'express';
import UserGroupController from '../controllers/UserGroup.controller';

const userGroupRouter = Router();
const userGroupController = new UserGroupController();

userGroupRouter.get('/', userGroupController.getAll);
userGroupRouter.get('/:id', userGroupController.getOne);
userGroupRouter.put('/:id', userGroupController.updateOne);
userGroupRouter.post('/', userGroupController.createOne);

export default userGroupRouter;

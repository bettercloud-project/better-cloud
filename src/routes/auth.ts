import { Router, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import UserService, { LoginRequestBody } from '../services/User.service';
import { HttpBadRequest } from '../httpErrors';
import { ProcessEnv } from '../typeHelpers';

const authRouter = Router();

const userService = new UserService();

const { JWT_SECRET } = process.env as ProcessEnv;

authRouter.post('/login', async (req, res: Response, next) => {
  const { body } = req as Request & { body: LoginRequestBody | undefined };

  if (!body || !body?.password || !body?.email) {
    return next(new HttpBadRequest('Email and/or password missing'));
  }

  const user = await userService.getByLogin({ email: body.email, password: body.password });

  if (!user) {
    return next(new Error('401 Wrong credentials'));
  }
  const token = jwt.sign(user.toJSON(), JWT_SECRET);
  res.cookie('X-AUTH', token, {
    secure: true,
    expires: new Date('2090.01.10'),
    sameSite: 'none',
    signed: false,
  });
  return res.send({ token });
});

export default authRouter;

import { Router } from 'express';
import Busboy from 'busboy';
import AWSService from '../services/AWS.service';

import { HttpBadRequest, HttpServerError } from '../httpErrors';
import { prepareS3Path } from '../helpers';
import AuthMiddleware from '../middlewares/Auth.middleware';
import RedisService from '../services/Redis.service';
import PublicFilesMiddleware from '../middlewares/PublicFiles.middleware';
import FolderService from '../services/Folder.service';
import UserService from '../services/User.service';

const filesRouter = Router();
const awsService = new AWSService();
const folderService = new FolderService();
const userService = new UserService();

filesRouter.get('/', async (req, res, next) => {
  if (!req.query.path) {
    return next(new HttpBadRequest('Unrecognized path parameter'));
  }
  const data = await awsService.listObjectsDeep(req.query.path.toString());
  const isAuthExists = await userService.getByToken(req.headers.authorization || req.cookies?.['X-AUTH']);
  if (isAuthExists) {
    return res.send(data);
  }
  const publicFolders = await folderService.findMany({ type: 'public' });
  const publicFolderPaths = publicFolders.map((f) => f.path);
  const publicData = data.data
    .filter(
      (entry) => publicFolderPaths
        .some((path) => entry.Key!.startsWith(path) || path.startsWith(entry.Key!)),
    );
  return res.send({ data: publicData });
});

filesRouter.get('/download', PublicFilesMiddleware, async (req, res) => {
  const { fileKey } = req.query as {fileKey: string};
  try {
    const {
      stream: fileStream,
      headers,
    } = await awsService.downloadStream(fileKey, req.headers.range);

    fileStream.on('error', () => res.status(500).send('File reading error'));
    fileStream.once('data', () => {
      const fileName = ([...fileKey.split('/')].pop()?.replace(/\s/g, '_')) || 'file';
      res.attachment(fileName);
      const { range = '' } = req.headers;
      const parts = range.replace(/bytes=/, '').split('-');
      const partialStart = parts[0];
      const partialEnd = parts[1];
      const total = parseInt(headers['content-length'], 10);
      const start = parseInt(partialStart, 10);
      const end = partialEnd ? parseInt(partialEnd, 10) : total - 1;
      const responseHeaders = [
        ['content-ranges', `bytes ${start}-${end}/${total}`],
        ['accept-ranges', headers['accept-ranges']],
        ['content-length', headers['content-length']],
        ['content-type', headers['content-type']],
        ['accept-encoding', headers['accept-encoding']],
      ];

      res.set(Object.fromEntries(responseHeaders.filter(([, value]) => value !== undefined)));
    });
    fileStream.on('data', async (chunk) => {
      res.write(chunk);
    })
      .on('end', res.end.bind(res));
  } catch (error) {
    res.status(500).send(error);
  }
});

filesRouter.post('/upload', AuthMiddleware, async (req, res, next) => {
  const { path } = req.query as {path: string};
  if (!req.path || !req.headers['content-type']?.includes('multipart/form-data')) {
    return next(new HttpBadRequest());
  }

  try {
    const busboy = new Busboy({ headers: req.headers });
    busboy
      .on('file', async (_fieldname, file, filename, _encoding, _mimetype) => {
        const awsFileKey = prepareS3Path(path, filename);
        console.log('fileKey - ', awsFileKey);
        const result = await awsService.uploadFile(awsFileKey, file);
        console.log('Upload complete!');
        return res.send(result);
      });

    req.pipe(busboy);
  } catch (error: any) {
    return next(new HttpServerError(error.message));
  }
  return null;
});

filesRouter.delete('/', AuthMiddleware, async (req, res, next) => {
  const { fileKey } = req.query as {fileKey: string};
  if (!fileKey) {
    return next(new HttpBadRequest());
  }
  try {
    const result = await awsService.removeFile(fileKey);
    return res.send(result);
  } catch (error: any) {
    return next(new HttpBadRequest(error.message));
  }
});

filesRouter.delete('/cache', async (req, res, next) => {
  const { path } = req.query as {path: string};
  if (!path) {
    return next(new HttpBadRequest());
  }
  try {
    const result = await RedisService.removeKeysLike(path);
    return res.send(result.toString());
  } catch (error: any) {
    return next(new HttpServerError(error.message));
  }
});

export default filesRouter;

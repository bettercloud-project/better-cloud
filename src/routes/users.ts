import { Router } from 'express';
import UserController from '../controllers/User.controller';

const usersRouter = Router();
const userController = new UserController();

usersRouter.get('/', userController.getAll);
usersRouter.get('/:id', userController.getOne);
usersRouter.post('/', userController.createOne);
usersRouter.put('/:id', userController.updateOne);

export default usersRouter;

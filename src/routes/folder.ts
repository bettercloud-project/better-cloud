import { Router } from 'express';
import FolderController from '../controllers/Folder.controller';

const folderRouter = Router();
const folderController = new FolderController();

folderRouter.get('/', folderController.findMy);
folderRouter.get('/:id', folderController.getOneMy);
folderRouter.post('/', folderController.createMy);
folderRouter.put('/:id', folderController.updateOneMy);
folderRouter.delete('/:id', folderController.deleteOneMy);

export default folderRouter;

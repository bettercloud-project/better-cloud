import { Router } from 'express';
import filesRouter from './files';
import authRouter from './auth';
import userGroupRouter from './userGroups';
import usersRouter from './users';
import AuthMiddleware from '../middlewares/Auth.middleware';
import folderRouter from './folder';
import { HttpNotFoundError } from '../httpErrors';

const router = Router();

router.use('/files', filesRouter);
router.use('/auth', authRouter);
router.use('/user-groups', AuthMiddleware, userGroupRouter);
router.use('/users', AuthMiddleware, usersRouter);
router.use('/folders', AuthMiddleware, folderRouter);
router.use('*', (req, res, next) => {
  next(new HttpNotFoundError(`Path ${req.originalUrl} not found`));
});

export default router;

import * as Knex from 'knex';
import bcrypt from 'bcrypt';
import User from '../src/models/User';

const { SUPER_ADMIN_EMAIL, SUPER_ADMIN_PASSWORD } = process.env;

export async function seed(knex: Knex): Promise<any> {
  const superAdmin: Partial<User> = {
    email: SUPER_ADMIN_EMAIL,
    password: await bcrypt.hash(SUPER_ADMIN_PASSWORD, 12),
    created_at: new Date().toISOString(),
    updated_at: new Date().toISOString(),
    firstName: 'admin',
  };

  // Deletes ALL existing entries
  await knex('users').del();
  await knex('userGroups').del();
  await knex('users_userGroups').del();
  await knex('users').insert(superAdmin);
  await knex('userGroups').insert({ name: 'superAdmin', created_at: new Date().toISOString(), updated_at: new Date().toISOString() });
  const user = await knex('users').select('*').first();
  const group = await knex('userGroups').select('*').first();
  await knex('users_userGroups').insert({ userId: user.id, groupId: group.id });
}

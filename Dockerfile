FROM node:20-alpine

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY package*.json ./

USER node

RUN npm ci

COPY --chown=node:node . .

ARG NODE_OPTIONS=--max_old_space_size=4096

RUN ["npm", "run", "build"]

CMD [ "npm", "start" ]
require('dotenv').config();
// import 'dotenv/config';

const options = {
  client: 'pg',
  connection: {
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port: Number(process.env.DB_PORT),
  },
};

// knex cli workaround
module.exports = {
  development: options,
};

// export default options;

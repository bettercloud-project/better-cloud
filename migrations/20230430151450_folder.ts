import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('folder', (table) => {
    table.increments('id').primary();
    table.string('path').notNullable();
    table.string('type').notNullable();
    table.timestamps();
    table
      .integer('userId')
      .unsigned()
      .references('id')
      .inTable('users')
      .onDelete('CASCADE')
      .index();
  });
}

export async function down(knex: Knex): Promise<void> {
  const q = knex.schema
    .dropTable('folder');
  return q;
}

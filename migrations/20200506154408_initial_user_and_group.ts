import * as Knex from 'knex';


export async function up(knex: Knex): Promise<any> {
  return knex.schema
    .createTable('users', (table) => {
      table.increments('id').primary();
      table.string('firstName');
      table.string('lastName');
      table.string('email').notNullable();
      table.string('password').notNullable();
      table.timestamps();
    })
    .createTable('userGroups', (table) => {
      table.increments('id').primary();
      table.string('name').notNullable();
      table.timestamps();
    })
    .createTable('users_userGroups', (table) => {
      table.increments('id').primary();

      table
        .integer('userId')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('CASCADE')
        .index();

      table
        .integer('groupId')
        .unsigned()
        .references('id')
        .inTable('userGroups')
        .onDelete('CASCADE')
        .index();
    });
}


export async function down(knex: Knex): Promise<any> {
  // knex.raw('SET foreign_key_checks = 0;');
  const q = knex.schema
    .dropTable('users_userGroups')
    .dropTable('users')
    .dropTable('userGroups');
  // knex.raw('SET foreign_key_checks = 1;');
  return q;
}
